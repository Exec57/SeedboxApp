import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';
import { ProgressbarModule } from 'ngx-bootstrap';
import { WebStorageModule } from 'ngx-store';

import { AppComponent } from './app.component';
import { TorrentService } from './_services/torrent.service';
import { TorrentsComponent } from './torrents/torrents.component';

@NgModule({
   declarations: [
      AppComponent,
      TorrentsComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      ModalModule.forRoot(),
      ProgressbarModule.forRoot(),
      WebStorageModule
   ],
   providers: [
      TorrentService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
