using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Seedbox_API.Models;

namespace Seedbox_API.Helpers
{
    public static class Utility
    {
        public static string FormatBytes(long bytes)
        {
            string[] suffix = { "o", "Ko", "Mo", "Go", "To" };
            int i;
            double dblSByte = bytes;
            for (i = 0; i < suffix.Length && bytes >= 1024; bytes /= 1024)
            {
                dblSByte = bytes / 1024.0;
                i++;
            }

            return String.Format("{0:0.##} {1}", dblSByte, suffix[i]);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static async Task<string> CommandAsync(string name, string args)
        {
            var start = new ProcessStartInfo();
            start.FileName = name;
            start.Arguments = args;
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            start.StandardOutputEncoding = System.Text.Encoding.UTF8;

            string result;
            using (var process = Process.Start(start))
            {
                using (var reader = process.StandardOutput)
                {
                    result = await reader.ReadToEndAsync();
                }
            }

            return result;
        }

        public static async Task<string> XmlRpcAsync(User user, string method, string target = "")
        {
            string args = string.Format("xmlrpc.py {0} {1} {2} {3}", user.Name, user.Password, method, target);
            string result = await Utility.CommandAsync("python", args);
            if (!string.IsNullOrEmpty(result))
                result = result.Remove(result.Length - 1);
            return result;
        }
    }
}