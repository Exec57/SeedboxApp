using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Seedbox_API.Data;
using Seedbox_API.Dtos;
using Seedbox_API.Helpers;
using Seedbox_API.Models;

namespace Seedbox_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TorrentsController : ControllerBase
    {
        private readonly ITorrentRepository _repo;
        private readonly IMapper _mapper;

        public TorrentsController(ITorrentRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var user = await HttpContext.GetUserAsync();

            await user.SemaphoreSlim.WaitAsync();
            var torrents = await _repo.GetTorrentsAsync(user);
            user.SemaphoreSlim.Release();

            if (torrents == null)
                return BadRequest();

            var torrentsDto = _mapper.Map<IEnumerable<TorrentDto>>(torrents);

            return Ok(torrentsDto);
        }

        [HttpDelete("{hash}")]
        public async Task<IActionResult> RemoveAsync(string hash)
        {
            var user = await HttpContext.GetUserAsync();

            await user.SemaphoreSlim.WaitAsync();
            await _repo.RemoveTorrentAsync(user, hash);
            user.SemaphoreSlim.Release();

            return Ok();
        }

        [HttpPost("file")]
        public async Task<IActionResult> AddFileAsync()
        {
            var user = await HttpContext.GetUserAsync();

            var file = HttpContext.Request.Form.Files[0];

            await user.SemaphoreSlim.WaitAsync();
            var torrent = await _repo.AddFileTorrentAsync(user, file);
            user.SemaphoreSlim.Release();

            if (torrent == null)
                return Ok();

            var torrentsDto = _mapper.Map<TorrentDto>(torrent);

            return Ok(torrentsDto);
        }

        [HttpPost("magnet")]
        public async Task<IActionResult> AddMagnetAsync(Magnet magnet)
        {
            var user = await HttpContext.GetUserAsync();

            await user.SemaphoreSlim.WaitAsync();
            var torrent = await _repo.AddMagnetTorrentAsync(user, magnet.Value);
            user.SemaphoreSlim.Release();

            if (torrent == null)
                return Ok();

            var torrentsDto = _mapper.Map<TorrentDto>(torrent);

            return Ok(torrentsDto);
        }
    }
}