using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Seedbox_API.Dtos;
using Seedbox_API.Helpers;

namespace Seedbox_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        [HttpPost("login")]
        public async Task<IActionResult> LogInAsync()
        {
            var user = await HttpContext.GetUserAsync();

            var userDto = new UserDto
            {
                Name = user.Name
            };

            return Ok(userDto);
        }

        [HttpPost("logout")]
        public async Task<IActionResult> LogOutAsync()
        {
            return await Task.Run(Unauthorized);
        }

        [HttpPost("restart")]
        public async Task<IActionResult> RestartAsync()
        {
            var user = await HttpContext.GetUserAsync();

            if (!user.IsRestarting)
            {
                user.IsRestarting = true;
                await user.SemaphoreSlim.WaitAsync();
                await Utility.CommandAsync("/bin/bash", "-c \"rm -f /home/" + user.Name + "/.session/rtorrent.lock\"");
                await Utility.CommandAsync("/bin/bash", "-c \"/etc/init.d/" + user.Name + "-rtorrent restart\"");
                user.SemaphoreSlim.Release();
                user.IsRestarting = false;
            }

            return Ok();
        }
    }
}